/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import juego.JuegoTriqui;

/**
 *
 * @author Pedro Luis
 */
public class ServidorMain 
{

    /**
     * @param args the command line arguments
     */
    private DefaultListModel mensajes = new DefaultListModel();
    private JuegoTriqui juego = new JuegoTriqui();
    public ServidorMain()
    {
        final int port = 1991;
        try
        {
            ServerSocket server = new ServerSocket(port);
            System.out.println("Servidor iniciado: Port="+port);
            while(true)
            {
                Socket client = server.accept();
                Runnable newClient = new ServidorTriqui(client, juego, mensajes);
                Thread thread = new Thread(newClient);
                thread.start();
            }
        }
        catch(IOException ex)
        {
            System.out.println("Error servidor: "+ ex.getMessage());
        }
    }
    public static void main(String[] args) 
    {
        // TODO code application logic here
        new ServidorMain();
    }
}
