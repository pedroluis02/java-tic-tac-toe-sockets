/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import adicional.Constantes;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import juego.JuegoTriqui;
import juego.JugadorTriqui;

/**
 *
 * @author Pedro Luis
 */
public class ServidorTriqui implements Runnable, ListDataListener, Constantes
{
    private DefaultListModel mensajesClientes;
    private JuegoTriqui juego;
    private DataInputStream dis;
    private DataOutputStream dos;
    private Socket socket;
    public ServidorTriqui(Socket socket, JuegoTriqui juego, DefaultListModel listm) 
    {
        this.mensajesClientes = listm;
        this.socket = socket;
        this.juego = juego;
        try
        {
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            mensajesClientes.addListDataListener(this);
        }
        catch(IOException ie)
        {
            System.out.println("Error server: "+ ie.getMessage());
        }
    }
    @Override
    public void run()
    {
        try
        {
            while(true)
            {
                int opcion = dis.readInt();
                if(opcion==UNIRSE)
                {
                    long idUser = dis.readLong();
                    int numJug; boolean turno;
                    if(juego.numeroJuego==-1)
                    {
                       juego.numeroJuego = 1;
                       numJug = 1;
                       turno = juego.jugador1.turno;
                    }
                    else
                    {
                       numJug = 2;
                       juego.iniciado = true;
                       turno = juego.jugador2.turno;
                    }
                    dos.writeInt(UNIRSE);
                    dos.writeLong(idUser);
                    dos.writeInt(numJug);
                    dos.writeInt(juego.numeroJuego);
                    dos.writeBoolean(turno);
                }
                else if(opcion==JUGADA)
                {
                   int numeroJugador = dis.readInt();
                   int numeroJuego = dis.readInt();
                   int posicion = dis.readInt();
                   int otroJ, tipo1, tipo2;
                   if(numeroJugador==1)
                   {
                      juego.tablero.setIconTablero(juego.jugador1, posicion);
                      otroJ = 2;
                   }
                   else
                   {
                      juego.tablero.setIconTablero(juego.jugador2, posicion);
                      otroJ = 1;
                   }
                   juego.intercambiarTurnos();
                   tipo1 = MITURNO;
                   tipo2 = ESPERANDO;
                   if(juego.tablero.isJuegoCompleto(numeroJugador))
                   {
                          tipo2 = GANADOR;
                          tipo1 = PERDEDOR;
                          juego.numeroJuego = -1;
                          juego.reiniciarJuego();
                   }
                   else if(juego.tablero.posicionesLibres==0)
                   {
                          tipo1 = EMPATE;
                          tipo2 = EMPATE;
                          juego.numeroJuego = -1;
                   }
                   synchronized(mensajesClientes)
                   {
                       mensajesClientes.addElement(
                               new Integer[]{TURNO, otroJ, numeroJuego, posicion, tipo1, tipo2});
                       System.out.println("Jugada: "+numeroJuego+"-"+numeroJugador+"-"+posicion);
                   }
                }
            }
        }
        catch(IOException ie)
        {
            return ;
        }
    }
    @Override
    public void intervalAdded(ListDataEvent e) 
    {
        Integer mensaje[] = (Integer[])mensajesClientes.getElementAt(e.getIndex0());
        try
        {
            dos.writeInt(mensaje[0]);
            dos.writeInt(mensaje[1]);
            dos.writeInt(mensaje[2]);
            dos.writeInt(mensaje[3]);
            dos.writeInt(mensaje[4]);
            dos.writeInt(mensaje[5]);
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
    }
    @Override
    public void intervalRemoved(ListDataEvent e) 
    {   
    }
    @Override
    public void contentsChanged(ListDataEvent e) 
    {
        
    }
}
