/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package adicional;

/**
 *
 * @author pedroluis
 */
public interface Constantes 
{
    public final int ESTADO = 1;
    public final int TURNO = 2;
    public final int JUGADA = 3;
    public final int UNIRSE = 4;
    
    public final int GANADOR = 10;
    public final int PERDEDOR = 11;
    public final int EMPATE = 12;
    
    public final int MITURNO = 20;
    public final int ESPERANDO = 21;
    
    public final int CONECTADO = 30;
    public final int DESCONECTADO = 31;
}
