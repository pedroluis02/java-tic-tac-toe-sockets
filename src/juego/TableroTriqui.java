/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author Pedro Luis
 */
public class TableroTriqui 
{
    public int matrizTriqui[][];
    public int posicionesLibres;
    public TableroTriqui()
    {
        matrizTriqui = new int[][]{{0,0,0},{0,0,0},{0,0,0}};
        posicionesLibres = 9;
    }
    public int getFila(int pos)
    {
        if(pos==1 || pos==2 || pos==3)
            return 0;
        else if(pos==4 || pos==5 || pos==6)
            return 1;
        else if(pos==7 || pos==8 || pos==9)
            return 2;
        else
            return -1;
    } 
    public int getColumna(int pos)
    {
        if(pos==1 || pos==4 || pos==7)
            return 0;
        else if(pos==2 || pos==5 || pos==8)
            return 1;
        else if(pos==3 || pos==6 || pos==9)
            return 2;
        else
            return -1;
    }
    public boolean isPosicionVacia(int pos)
    {
        int row = getFila(pos);
        int column = getColumna(pos);
        
        if(row==-1 || column==-1)
           return false;
        if(matrizTriqui[row][column]==0)
           return true;
        else
           return false;
    }
    public void reiniciarTablero()
    {
        posicionesLibres = 9;
        for(int row=0; row<3; row++){
            for(int column=0; column<3; column++)
            {
                matrizTriqui[row][column] = 0;
            }
        }
    }
   
    public void setIconTablero(JugadorTriqui jugador, int pos)
    {
        int fila = getFila(pos);
        int column = getColumna(pos);
        
        matrizTriqui[fila][column] = jugador.numeroJugador;
        posicionesLibres = posicionesLibres - 1;
    }
    public int getNumberPlayer(int pos)
    {
        int row = getFila(pos);
        int column = getColumna(pos);
        
        return matrizTriqui[row][column];
    }
    public boolean isJuegoCompleto(int number)
    {
        //* Comprueba las lineas horizontales */
        if(matrizTriqui[0][0] == number && matrizTriqui[0][0] == matrizTriqui[0][1] && matrizTriqui[0][0] == matrizTriqui[0][2])
            return true;
        else if(matrizTriqui[1][0] == number && matrizTriqui[1][0] == matrizTriqui[1][1] && matrizTriqui[1][0] == matrizTriqui[1][2])
            return true;
        else if(matrizTriqui[2][0] == number && matrizTriqui[2][0] == matrizTriqui[2][1] && matrizTriqui[2][0] == matrizTriqui[2][2])
            return true;
        //* Comprueba las lineas verticales */
        else if(matrizTriqui[0][0] == number && matrizTriqui[0][0] == matrizTriqui[1][0] && matrizTriqui[0][0] == matrizTriqui[2][0])
            return true;
        else if(matrizTriqui[0][1] == number && matrizTriqui[0][1] == matrizTriqui[1][1] && matrizTriqui[0][1] == matrizTriqui[2][1])
            return true;
        else if(matrizTriqui[0][2] == number && matrizTriqui[0][2] == matrizTriqui[1][2] && matrizTriqui[0][2] == matrizTriqui[2][2])
            return true;
        //* Comprueba las lineas diagonales */
        else if(matrizTriqui[0][0] == number && matrizTriqui[0][0] == matrizTriqui[1][1] && matrizTriqui[0][0] == matrizTriqui[2][2])
            return true;
        else if(matrizTriqui[2][0] == number && matrizTriqui[2][0] == matrizTriqui[1][1] && matrizTriqui[2][0] == matrizTriqui[0][2])
            return true;
        else
            return false;
    }
}
