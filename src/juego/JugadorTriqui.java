/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author Pedro Luis
 */
public class JugadorTriqui 
{
    public int numeroJugador;
    public int numeroJuego;
    public String nick;
    public char icon;
    public boolean turno;
    public boolean asignado;
    public boolean conectado;
    public JugadorTriqui()
    {
        this(-1, "",'.', false, -1);
    }
    public JugadorTriqui(int number, String nick, char icon, boolean turn, int numberGame)
    {
        this.numeroJugador = number;
        this.nick = nick;
        this.icon = icon;
        this.turno = turn;
        this.asignado = false;
        this.numeroJuego = numberGame;
        this.conectado = false;
    } 
    public void reinicarJugador(int number,char icon, boolean turn) 
    {
        this.numeroJugador = number;
        this.icon = icon;
        this.turno = turn;
        this.asignado = false;
    }
}
