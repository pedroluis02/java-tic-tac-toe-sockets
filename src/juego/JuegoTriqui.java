/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package juego;

/**
 *
 * @author Pedro Luis
 */
public class JuegoTriqui 
{
    public int numeroJuego;
    public boolean iniciado;
    public boolean finalizado;
    public boolean esperando;
    public TableroTriqui tablero;
    public JugadorTriqui jugador1;
    public JugadorTriqui jugador2;
            
    public JuegoTriqui() 
    {
        this.numeroJuego = -1;
        this.iniciado = false;
        this.finalizado = false;
        this.esperando = false;
        this.tablero = new TableroTriqui();
        jugador1 = new JugadorTriqui(1, "p1", 'x',  true, numeroJuego);
        jugador2 = new JugadorTriqui(2, "p2", '0', false, numeroJuego);
    }
    public JuegoTriqui(int number)
    {
        this.numeroJuego = number;
        this.iniciado = false;
        this.finalizado = false;
        this.esperando = false;
        this.tablero = new TableroTriqui();
        jugador1 = new JugadorTriqui(1, "p1", 'x', true, number);
        jugador2 = new JugadorTriqui(2, "p2", '0', false, number);
    }
    public void reiniciarJuego()
    {
        iniciado = false;
        finalizado = false;
        esperando = false;
        tablero.reiniciarTablero();
        jugador1.reinicarJugador(1, 'x', true);
        jugador2.reinicarJugador(2, '0', false);
    }
    //disponible
    public boolean isDisponible()
    {
        if(iniciado==false)
            return true;
        else
            return false;
    }
    //esperando
    public boolean isEsperando()
    {
        if(esperando==true)
            return true;
        else
            return false;
    }
    //reiniciar juego
    public JugadorTriqui reiniciarNuevoJuego()
    {
        esperando = true;
        if(jugador1.asignado==false)
        {
           jugador1.asignado = true;
            return jugador1;
        }
        else if(jugador2.asignado==false)
        {
            jugador2.asignado = true;
            return jugador2;
        }
        return null;
    }
    //unirse
    public JugadorTriqui unirseJuego()
    {
        esperando = false;
        iniciado = true;
        // Retorna el jugador que no esta asignado
        if(jugador1.asignado==false)
        {
            jugador1.asignado = true;
            return jugador1;
        }
        else if(jugador2.asignado==false)
        {
            jugador2.asignado = true;
            return jugador2;
        }
        return null;
    }  
    //intercambiar turnos
    public void intercambiarTurnos()
    {
        if(jugador1.turno==true)
        {
            jugador1.turno = false;
            jugador2.turno = true;
        } 
        else
        {
            jugador1.turno = true;
            jugador2.turno = false;
        }
    }
    public boolean isPosicionLibre(int pos)
    {
        return tablero.isPosicionVacia(pos);
    }
    public void setIconTablero(JugadorTriqui player, int pos)
    {
        tablero.setIconTablero(player, pos);
    }
    public boolean isJuegoCompleto()
    {
        if(finalizado || tablero.posicionesLibres<=0)
           return true;
        
        for (int i=1; i<=2; i++)
        {
            if(tablero.isJuegoCompleto(i))
               return true;
        }
        return false;
    }
    //comprobar juego
    public void comprobarJuego()
    {
        if(isJuegoCompleto())
        {
            finalizado = true;
            iniciado = false;
        }
        else if(tablero.posicionesLibres <= 0)
        {
            finalizado = true;
            iniciado = false;
        }
        else
        {
            finalizado = false;
        }
    }
    public void logica(JugadorTriqui player, int pos)
    {
        if(jugador1.turno)
        {
           setIconTablero(player, pos);
           intercambiarTurnos();
        }
        else if(jugador2.turno)
        {
          setIconTablero(player, pos);
          intercambiarTurnos();
        }
       comprobarJuego();
    }
}