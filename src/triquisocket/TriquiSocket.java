/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triquisocket;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 *
 * @author pedroluis
 */
public class TriquiSocket 
{

    /**
     * @param args the command line arguments
     */
    public TriquiSocket(String h)
    {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        new TriquiGui(h);
    }
    public static void main(String[] args) 
    {
        // TODO code application logic here
        String h = "localhost";
        if(args.length==1)
           h = args[0];
        new TriquiSocket(h);
    }
}
