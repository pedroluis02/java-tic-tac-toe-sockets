/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triquisocket;

/**
 *
 * @author Pedro Luis
 *
**/
import adicional.Constantes;
import java.awt.BorderLayout;
import juego.JugadorTriqui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import java.io.IOException;
import java.net.Socket;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.BorderUIResource;
import juego.TableroTriqui;

public class TriquiGui extends JFrame implements ActionListener
{
    private JPanel[] paneles;
    private JScrollPane panelDes;
    private Box[] boxs; 
    private JTextArea stateGame;
    private JButton[][] buttonsTR;
    
    private JMenuBar menuBar;
    private JMenu[] menus;
    private JMenuItem[] subMenus;
    
    private String host;
    private int port;
    public JugadorTriqui jugador; 
    public TableroTriqui tablero;
    
    private Socket socket;
    private ClientTriqui cliente;
    
    public TriquiGui(String h)
    {
        super("JUEGO TRIQUI");
        int_Menus();
        initComponents_TriquiTR();
        initEvent_TriquiGui();
        host = h;
        port = 1991;
        jugador = new JugadorTriqui();
        tablero = new TableroTriqui();
        if(conectarAlServidor());
           enviarMensajeJuegoDisponible();
    }
    private void initComponents_TriquiTR()
    {
        paneles = new JPanel[2];
        boxs = new Box[2];
        buttonsTR = new JButton[3][3];
        stateGame = new JTextArea();
        stateGame.setMargin(new Insets(25, 25, 25, 25));
        stateGame.setBackground(this.getBackground());
        stateGame.setEditable(false);
        panelDes = new JScrollPane(stateGame);
       panelDes.setPreferredSize(new Dimension(25, 25));
        panelDes.setBorder(BorderFactory.createTitledBorder("Jugador"));
        /*---------------------------------------------------------------*/
        paneles[0] = new JPanel(new GridLayout(3, 3, 10, 10));
//        paneles[0].setPreferredSize(new Dimension(200, 200));
        paneles[0].setBorder(BorderFactory.createTitledBorder("Juego Triqui"));
        
        for(int row=0; row<3; row++)
        {
            for(int column=0; column<3; column++)
            {
                buttonsTR[row][column] = new JButton();
                buttonsTR[row][column].setBackground(Color.white);
                buttonsTR[row][column].setPreferredSize(new Dimension(16 ,16));
                buttonsTR[row][column].setBorder(BorderFactory.createLineBorder(Color.blue));
                paneles[0].add(buttonsTR[row][column]);
            }
        }
        
        /*---------------------------------------------------------------*/
        boxs[0] = Box.createHorizontalBox();
        boxs[0].add(paneles[0], Box.CENTER_ALIGNMENT);
        boxs[0].add(panelDes, Box.RIGHT_ALIGNMENT);
        /*--------------------------------------------------------------*/
        paneles[1] = new JPanel(new FlowLayout(FlowLayout.CENTER));
        paneles[1].setBorder(BorderUIResource.getRaisedBevelBorderUIResource());
        /*--------------------------------------------------------------*/
        add(boxs[0], BorderLayout.CENTER);
        add(paneles[1], BorderLayout.SOUTH);
        
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((int)(d.width/8), (int)(d.height/8));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setSize(600, 400);
        //setResizable(false);
        setVisible(true);
//        pack();
    }
    private void int_Menus()
    {
        menuBar = new JMenuBar();
        menus = new JMenu[2];
        subMenus = new JMenuItem[4];
        
        menus[0] = new JMenu("Triqui");
        menus[0].setMnemonic('T');
        subMenus[0] = new JMenuItem("Salir"); 
        subMenus[0].setActionCommand("exit");
        subMenus[0].addActionListener(this);
        subMenus[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
        /*---------------------------*/
        menus[1] = new JMenu("Ayuda");
        menus[1].setMnemonic('A');
        subMenus[1] = new JMenuItem("A cerca de...");
        subMenus[1].setActionCommand("help");
        subMenus[1].addActionListener(this);
        /*---------------------------*/
        menus[0].add(subMenus[0]);
        menus[1].add(subMenus[1]);
        
        menuBar.add(menus[0]);
        menuBar.add(menus[1]);
        
        setJMenuBar(menuBar);
    }
    private void initEvent_TriquiGui()
    {
        buttonsTR[0][0].setActionCommand("00");
        buttonsTR[0][1].setActionCommand("01");
        buttonsTR[0][2].setActionCommand("02");
        buttonsTR[1][0].setActionCommand("10");
        buttonsTR[1][1].setActionCommand("11");
        buttonsTR[1][2].setActionCommand("12");
        buttonsTR[2][0].setActionCommand("20");
        buttonsTR[2][1].setActionCommand("21");
        buttonsTR[2][2].setActionCommand("22");
        
        for(int row=0; row<3; row++)
            for(int column=0; column<3; column++)
                buttonsTR[row][column].addActionListener(this);
    }
    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getActionCommand().compareTo("new")==0)
        {
            reiniciarIconosTablero();
            enviarMensajeJuegoDisponible();
        }
        else if(e.getActionCommand().compareTo("help")==0)
        {
        }
        else if(e.getActionCommand().compareTo("exit")==0)
        {
           if(desconectarDelServidor()) 
              System.exit(0);           
        }
        else if(e.getActionCommand().compareTo("00")==0)
           actionsButtons(1);
        else if(e.getActionCommand().compareTo("01")==0)
           actionsButtons(2);
        else if(e.getActionCommand().compareTo("02")==0)
           actionsButtons(3);
        else if(e.getActionCommand().compareTo("10")==0)
           actionsButtons(4);
        else if(e.getActionCommand().compareTo("11")==0)
           actionsButtons(5);
        else if(e.getActionCommand().compareTo("12")==0)
           actionsButtons(6);
        else if(e.getActionCommand().compareTo("20")==0)
           actionsButtons(7);
        else if(e.getActionCommand().compareTo("21")==0)
           actionsButtons(8);
        else if(e.getActionCommand().compareTo("22")==0)
           actionsButtons(9); 
    }
    /*-------------------------------------------------------------------*/
    private void mensajeErrorDialog(String mensaje)
    {
        JOptionPane.showMessageDialog(this , mensaje, "Mensaje de Error", 
                JOptionPane.ERROR_MESSAGE);
    }
    private void mensajeInformacionDialog(String mensaje)
    {
        JOptionPane.showMessageDialog(this , mensaje, "Mensaje Triqui", 
                JOptionPane.INFORMATION_MESSAGE);
    }
    /*-------------------------------------------------------------------*/
    private void actionsButtons(int position)
    {
        if(jugador.turno)
        {
           if(tablero.isPosicionVacia(position))
           {
               setIconTablero(jugador, position);
               enviarMensajeJugada(position);
               jugador.turno = false;
           }
           else
               mensajeErrorDialog("Posicion ocupada...");
        }
        else
           mensajeErrorDialog("No es tu turno...");
    }
    public void setIconTablero(JugadorTriqui jugador, int position)
    {
        ImageIcon icon = null;
        tablero.setIconTablero(jugador, position);
        if(jugador.icon=='x')
           icon = new ImageIcon(getClass().getResource("/iconos/equis.jpg"));
        else if(jugador.icon=='0')
           icon = new ImageIcon(getClass().getResource("/iconos/cero.jpg"));
        switch(position)
        {
            case 1: buttonsTR[0][0].setIcon(icon);
                    break;
            case 2: buttonsTR[0][1].setIcon(icon);
                    break;
            case 3: buttonsTR[0][2].setIcon(icon);
                    break;
            case 4: buttonsTR[1][0].setIcon(icon);
                    break;
            case 5: buttonsTR[1][1].setIcon(icon);
                    break;
            case 6: buttonsTR[1][2].setIcon(icon);
                    break;
            case 7: buttonsTR[2][0].setIcon(icon);
                    break;
            case 8: buttonsTR[2][1].setIcon(icon);
                    break;
            case 9: buttonsTR[2][2].setIcon(icon);
                    break;
        }
    }
    private void reiniciarIconosTablero()
    {
        buttonsTR[0][0].setIcon(null);
        buttonsTR[0][1].setIcon(null);
        buttonsTR[0][2].setIcon(null);
        buttonsTR[1][0].setIcon(null);
        buttonsTR[1][1].setIcon(null);
        buttonsTR[1][2].setIcon(null);
        buttonsTR[2][0].setIcon(null);
        buttonsTR[2][1].setIcon(null);
        buttonsTR[2][2].setIcon(null);
        stateGame.setText("");
        tablero.reiniciarTablero();
    }    
    /*-------------------------------------------------------------*/
    public void mostrarMensajeState(int tipo)
    {
        switch(tipo)
        {
            case Constantes.MITURNO: stateGame.append("Tu turno...\n"); 
                                      break;
            case Constantes.ESPERANDO: stateGame.append("Esperando...\n"); 
                                       break;
            case Constantes.GANADOR: mensajeInformacionDialog("Has ganado...");
                                     reiniciarIconosTablero();
                                     enviarMensajeJuegoDisponible();
                                     break;
            case Constantes.PERDEDOR: mensajeInformacionDialog("Has perdido....");
                                      reiniciarIconosTablero();
                                      enviarMensajeJuegoDisponible();
                                      break;
            case Constantes.EMPATE: mensajeInformacionDialog("Juego Empatado...");
                                    
                                    reiniciarIconosTablero();
                                    enviarMensajeJuegoDisponible();
                                    break;
        }
    }
    /*------------------------------------------------------------*/
    private boolean conectarAlServidor()
    {   
        try
        {
            socket = new Socket(host, port);
            cliente = new ClientTriqui(socket, this);
        }
        catch(IOException e)
        {
            mensajeErrorDialog("Error al conectar al servidor...");
            System.exit(0);
        }
        return true;
    }
    private boolean desconectarDelServidor() 
    {
        try
        {
            if(socket!=null && cliente!=null)
            {
               socket.close();
               cliente.cerrarCliente();
            }
        }
        catch(IOException e)
        {
            mensajeErrorDialog("No se pudo desconectar...");
            return false;
        }
        enviarMessajeText("Se desconecto...");
        return true;
    }
    private void enviarMessajeText(String messageText)
    {
        try 
        {
            cliente.enviarMensajeText(messageText);
        } 
        catch (IOException ex)
        {
            mensajeErrorDialog("No se pudo enviar mensaje...");
        }
    }
    private void enviarMensajeJugada(int pos)
    {
        try
        {
            cliente.enviarMensajeJugada(jugador, pos);
        }
        catch(IOException e)
        {
            mensajeErrorDialog("No se pudo realizar jugadada...");
        }
    }
    /*------------------------------------------------------------*/
    private void enviarMensajeJuegoDisponible()
    {
        try 
        {
            cliente.enviarMesajeJuegoDisponible();
        } 
        catch (IOException ex) 
        {
            mensajeErrorDialog("Error: "+ex);
            return;
        }
    }
    /*-------------------------------------------------------------*/
    public void actualizarNombreJugador(int num, char icon)
    {
        panelDes.setBorder(BorderFactory.createTitledBorder("Jugador "+num+
                " ["+icon+"]"));
    }
}