/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triquisocket;

import adicional.Constantes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import juego.JugadorTriqui;

/**
 *
 * @author pedroluis
 */
public class ClientTriqui implements Runnable, ActionListener, Constantes
{
    private TriquiGui triquigui;
    private DataInputStream dis;
    private DataOutputStream dos;
    private long idUser = new Date().getTime();
    public ClientTriqui(Socket socket, TriquiGui triquigui)
    {
        this.triquigui = triquigui;
        try
        {
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            Thread thread = new Thread(this);
            thread.start();
        }
        catch(IOException ie)
        {
            System.out.println("Error Client:"+ ie.getMessage());
        }
        System.out.println("\n Id de Usuario: "+idUser);
    }
    public void enviarMensajeText(String messageText) throws IOException
    {
       dos.writeInt(7);
       dos.writeUTF(triquigui.jugador.nick+": "+messageText);
    }
    public void enviarMensajeEstado(String mensajeEstado) throws IOException
    {
        dos.writeInt(ESTADO);
        dos.writeInt(triquigui.jugador.numeroJugador);
        dos.writeUTF(mensajeEstado);
    }
    public void enviarMensajeJugada(JugadorTriqui jugador, int pos) throws IOException
    {
        dos.writeInt(JUGADA);
        dos.writeInt(jugador.numeroJugador);
        dos.writeInt(jugador.numeroJuego);
        dos.writeInt(pos);
    }
    public void enviarMesajeJuegoDisponible() throws IOException
    {
        dos.writeInt(UNIRSE);
        dos.writeLong(idUser);
    }
    public boolean cerrarCliente() 
    {
        try 
        {
            dis.close();
            dos.close();
        } 
        catch (IOException ex) 
        {
            return false;
        }
        return true;
    }
    @Override
    public void run() 
    {
        try
        {
            while(true)
            {
                int opcion = dis.readInt();
                if(opcion==UNIRSE)
                {
                   long id = dis.readLong();
                   if(id==idUser)
                   {
                      triquigui.jugador.numeroJugador = dis.readInt();
                      triquigui.jugador.numeroJuego = dis.readInt();
                      boolean turno = dis.readBoolean();
                      if(triquigui.jugador.numeroJugador==1)
                         triquigui.jugador.icon = 'x';
                      else
                         triquigui.jugador.icon = '0';
                      triquigui.jugador.turno = turno;
                      if(turno)
                         triquigui.mostrarMensajeState(MITURNO);
                      else
                         triquigui.mostrarMensajeState(ESPERANDO); 
                      triquigui.actualizarNombreJugador(triquigui.jugador.numeroJugador, 
                              triquigui.jugador.icon);
                      System.out.println("Nº Jugador: "+triquigui.jugador.numeroJugador);
                      System.out.println("Nº Juego: "+triquigui.jugador.numeroJuego);
                   }
                }
                else if(opcion==TURNO)
                {
                    int NumJugador = dis.readInt();
                    int numJuego = dis.readInt();
                    int otroPos = dis.readInt();
                    int tipo1 = dis.readInt();
                    int tipo2 = dis.readInt();
                    if(NumJugador==triquigui.jugador.numeroJugador &&
                       numJuego==triquigui.jugador.numeroJuego)
                    {
                       JugadorTriqui tem = new JugadorTriqui();
                       if(NumJugador==1)
                          tem.icon = '0';
                       else
                          tem.icon = 'x';
                       
                       triquigui.setIconTablero(tem, otroPos);
                       triquigui.mostrarMensajeState(tipo1);
                       triquigui.jugador.turno = true;
                    }
                    else
                        triquigui.mostrarMensajeState(tipo2);
                }
            }
        }
        catch(IOException ex)
        {
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) 
    {
    }
}
